# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include grupo01_zuul
class grupo01_zuul {
exec { 'refresh_zuul':  
	command => "/usr/bin/systemctl restart zuul",  
	refreshonly => true,
  }
exec { 'refresh_daemon':  
	command => "/usr/bin/systemctl daemon-reload",  
	refreshonly => true,
  }
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/zuul':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/zuul.service":
        mode => "0644",        
	owner => 'root',       
	group => 'root',        
	source => 'puppet:///modules/grupo01_zuul/zuul.service',     
	notify => Exec['refresh_daemon'],
  }
file { "/opt/apps/zuul/application.properties":        
	mode => "0644",        
	owner => 'java',        
	group => 'java',        
	source => 'puppet:///modules/grupo01_zuul/bootstrap.txt',    
  }
package { 'maven':
	ensure => installed,
	name => $maven,
  }
remote_file { '/opt/apps/zuul/zuul.jar':
    ensure => latest,
    owner => java,
    group => java,
    source => 'http://23.96.48.125/artfactory/grupo01/zuul/zuul.jar',
  }
service { 'zuul_service':
	name => zuul,
	enable => true,
	start => true,
 }
}

